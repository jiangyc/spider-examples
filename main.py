# -*- coding: utf-8 -*-
from flask import Flask

from util import get_config, get_logger

# Controllers
from controller import *
# Jobs
from job import *


app = Flask(__name__)
app.register_blueprint(global_controller)


@app.route("/")
def index() -> str:
    return "Hello world!"


if __name__ == '__main__':
    log = get_logger()

    flask_config = get_config('flask')
    flask_host = flask_config.get('host', '127.0.0.1')
    flask_port = flask_config.get('port', 8000)
    flask_debug = flask_config.get('debug', False)

    global_scheduler.init_app(app)
    global_scheduler.start()

    log.debug("启动成功！")

    app.run(host=flask_host, port=flask_port, debug=flask_debug)
