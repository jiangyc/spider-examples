# -*- coding: utf-8 -*-
import json
import os.path


def load_config(env=None) -> dict:
    # 加载默认判断配置
    config_path = join_cwd_path("config.json")
    config_dict = load_json(config_path)
    if not config_dict:
        raise FileNotFoundError(f"配置文件[{config_path}]为空或不存在！")
    marge_config_dict: dict = config_dict.copy()

    # 获取env字段，默认为prod
    if not env:
        env = config_dict.get("env")
        if not env:
            env = "prod"

    # 加载运行环境配置
    env_config_path = join_cwd_path(f"config-{env}.json")
    env_config_dict = load_json(env_config_path)
    if not env_config_dict:
        print("配置文件[{env_config_path}]为空或不存在！")
    else:
        for key, value in env_config_dict.items():
            if not value:
                continue
            if isinstance(value, dict):
                if not marge_config_dict.get(key) or not isinstance(marge_config_dict[key], dict):
                    marge_config_dict[key] = {}
                marge_config_dict[key].update(value)
            elif isinstance(value, list):
                if not marge_config_dict.get(key) or not isinstance(marge_config_dict[key], list):
                    marge_config_dict[key] = []
                marge_config_dict[key].extend(value)
            else:
                marge_config_dict[key] = value
    return marge_config_dict


def get_config(key: str, default_value=None, env=None, config_dict=None) -> any:
    if not config_dict:
        config_dict = load_config(env)
    if not config_dict:
        return default_value
    if not key:
        return default_value
    if not isinstance(key, str):
        raise ValueError(f"配置项[{key}]的key不是str类型！")
    return config_dict.get(key, default_value)
    # if "." not in key:
    #     return config_dict.get(key, default_value)
    # key_list = key.split(".")
    # if len(key_list) < 2:
    #     return config_dict.get(key, default_value)
    # value = config_dict.get(key_list[0])
    # if not value:
    #     return default_value
    # for i in range(1, len(key_list)):
    #     value = value.get(key_list[i])
    #     if not value:
    #         return default_value
    # return value


def get_config_dict(key: str, default_value: dict or None = None, env: str or None = None,
                    config_dict: dict or None = None) -> dict:
    config_value = get_config(key, default_value, env, config_dict)
    if config_value is not None and not isinstance(config_value, dict):
        raise ValueError(f"配置项[{key}]的值不是dict类型！")
    return config_value


def get_config_int(key: str, default_value: int or None = None, env: str or None = None,
                   config_dict: dict or None = None) -> int:
    config_value = get_config(key, default_value, env, config_dict)
    if config_value is not None and not isinstance(config_value, int):
        raise ValueError(f"配置项[{key}]的值不是int类型！")
    return config_value


def get_config_bool(key: str, default_value: dict or None = None, env: str or None = None,
                    config_dict: dict or None = None) -> bool:
    config_value = get_config(key, default_value, env, config_dict)
    if config_value is not None and not isinstance(config_value, bool):
        raise ValueError(f"配置项[{key}]的值不是bool类型！")
    return config_value


def get_config_str(key: str, default_value: dict or None = None, env: str or None = None,
                   config_dict: dict or None = None) -> str:
    config_value = get_config(key, default_value, env, config_dict)
    if config_value and not isinstance(config_value, str):
        raise ValueError(f"配置项[{key}]的值不是str类型！")
    return config_value


def join_cwd_path(file_name: str) -> str or None:
    if not file_name:
        return None
    cwd_path = os.path.dirname(os.path.dirname(__file__))
    return os.path.join(cwd_path, file_name)


def load_json(file_path: str) -> dict or None:
    if not file_path:
        return None
    if not os.path.exists(file_path):
        return None
    with open(file_path, 'r', encoding='utf-8') as f:
        return json.load(f)
