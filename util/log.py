# -*- coding: utf-8 -*-
import logging
import os

from util.config import get_config


def get_log_level_from_str(level: str) -> int:
    level = level.lower()
    if level == 'debug':
        return logging.DEBUG
    elif level == 'info':
        return logging.INFO
    elif level == 'warning':
        return logging.WARNING
    elif level == 'warn':
        return logging.WARNING
    elif level == 'error':
        return logging.ERROR
    elif level == 'critical':
        return logging.CRITICAL
    else:
        raise ValueError(f"日志级别[{level}]不合法！")


def get_logger(name: str = None):
    log = logging.getLogger(name)

    log_config = get_config('log', {})
    log_mode = log_config.get("mode", 'console')
    log_level = get_log_level_from_str(log_config.get("level", 'info'))
    log_formatter = log_config.get('formatter', '%(asctime)s - %(name)s [%(levelname)s] %(message)s')
    log.setLevel(log_level)

    if log_mode == 'file':
        log_file = log_config.get("file")
        if log_file is None:
            raise ValueError("日志文件未配置！")
        log_file_dirname = os.path.dirname(log_file)
        if not os.path.exists(log_file_dirname):
            os.makedirs(log_file_dirname)
        # 判断是否是一个可访问的目录
        if not os.path.isdir(log_file_dirname):
            raise ValueError(f"日志文件路径[{log_file_dirname}]不是一个目录！")

        handler = logging.FileHandler(log_file)
        handler.setLevel(log_level)
        handler.setFormatter(logging.Formatter(log_formatter))
        log.addHandler(handler)
    elif log_mode == 'console':
        handler = logging.StreamHandler()
        handler.setLevel(log_level)
        handler.setFormatter(logging.Formatter(log_formatter))
        log.addHandler(handler)

    return log
