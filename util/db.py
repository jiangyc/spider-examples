# -*- coding: utf-8 -*-
import datetime
import importlib.util
from typing import Optional, List

import cx_Oracle
import pymysql as pymysql
from pymysql.cursors import Cursor, DictCursor

from config import get_config
from util import get_logger


class Db:

    def __init__(self, db_config: dict, data_source: dict):
        self.db_config: dict = db_config
        self.data_source: dict = data_source

        # 参数：是否打印SQL
        self.is_print_sql: bool = False
        if self.db_config:
            print_sql = self.db_config.get("print_sql")
            if print_sql and isinstance(print_sql, bool):
                self.is_print_sql = print_sql
            elif print_sql and isinstance(print_sql, str):
                self.is_print_sql = print_sql.lower() == 'true'

    def get_data_source(self) -> dict:
        return self.data_source

    def query_list(self, sql) -> list[dict]:
        pass

    def query_one(self, sql) -> dict:
        pass

    def query_for_int(self, sql) -> int:
        pass

    def query_for_str(self, sql) -> str:
        pass

    def query_for_bool(self, sql):
        pass

    def query_for_float(self, sql) -> float:
        pass

    def update(self, sql) -> int:
        pass

    def smart_add(self, table_name: str, data: dict, ignore_none: bool = False) -> bool:
        """
        智能添加数据
        :param table_name: 表名
        :param data: 数据
        :param ignore_none: 是否忽略空值
        :return: 是否成功
        """
        if not table_name or not data:
            return False
        # 过滤空值
        data_to_insert = data
        if ignore_none:
            data_to_insert = {k: v for k, v in data.items() if v is not None}
        if not data_to_insert:
            return False
        # 构建SQL
        sql_fields = []
        sql_values = []
        for k, v in data_to_insert.items():
            sql_fields.append(k)
            sql_values.append(self.to_sql_value(v))
        sql = f"insert into {table_name} ({','.join(sql_fields)}) values ({','.join(sql_values)})"
        # 执行SQL
        return self.update(sql) > 0

    def smart_update(self, table_name: str, data: dict, where: dict, where_sql=None, ignore_none: bool = False) -> bool:
        """
        智能更新数据
        :param table_name: 表名
        :param data: 数据
        :param where: 条件，字典。当where_sql不为空时，where参数无效
        :param where_sql: 条件SQL，字符串。当where_sql不为空时，where参数无效
        :param ignore_none: 是否忽略数据中的空值
        :return: 是否成功
        """
        if not table_name or not data:
            return False
        if not where and not where_sql:
            return False
        # 过滤空值
        data_to_update = data
        if ignore_none:
            data_to_update = {k: v for k, v in data.items() if v is not None}
        if not data_to_update:
            return False
        # 构建SQL
        sql_fields = []
        for k, v in data_to_update.items():
            sql_fields.append(f"{k}={self.to_sql_value(v)}")
        if where_sql:
            sql = f"update {table_name} set {', '.join(sql_fields)} where {where_sql}"
        else:
            sql_where = []
            for k, v in where.items():
                sql_where.append(f"{k}={self.to_sql_value(v)}")
            sql = f"update {table_name} set {', '.join(sql_fields)} where {' and '.join(sql_where)}"
        # 执行SQL
        return self.update(sql) > 0

    def smart_add_or_update(self, table_name: str, data: dict, where: dict, where_sql = None,
                            ignore_none: bool = False) -> bool:
        """
        智能添加或更新数据
        :param table_name: 表名
        :param data: 数据
        :param where: 条件，字典。当where_sql不为空时，where参数无效
        :param where_sql: 条件SQL，字符串。当where_sql不为空时，where参数无效
        :param ignore_none: 是否忽略数据中的空值
        :return: 是否成功
        """
        if not table_name or not data:
            return False
        if not where and not where_sql:
            return self.smart_add(table_name, data, ignore_none)

        # 判断该记录是否存在
        if where_sql:
            sql_where = where_sql
        else:
            wheres = []
            for k, v in where.items():
                wheres.append(f"{k}={self.to_sql_value(v)}")
            sql_where = " and ".join(wheres)
        sql = f"select count(1) from {table_name} where {sql_where}"
        # 根据记录是否存在，执行添加或更新
        if self.query_for_int(sql) > 0:
            return self.smart_update(table_name, data, where, where_sql, ignore_none)
        else:
            return self.smart_add(table_name, data, ignore_none)

    @staticmethod
    def to_sql_value(v) -> str:
        """
        将Python值转换为SQL值
        :param v: Python值
        :return: SQL值
        """
        if isinstance(v, str):
            return f"'{v}'"
        elif isinstance(v, datetime.datetime):
            return f"'{v.strftime('%Y-%m-%d %H:%M:%S')}'"
        elif isinstance(v, datetime.date):
            return f"'{v.strftime('%Y-%m-%d')}'"
        elif isinstance(v, bool):
            return "1" if v else "0"
        elif v is None:
            return "NULL"
        else:
            return str(v)

    @staticmethod
    def get_enabled_platforms():
        """
        获取启用的平台列表
        :return: 启用的平台列表
        """
        enable_platforms = []
        if importlib.util.find_spec("cx_Oracle") is not None:
            enable_platforms.append("oracle")
        if importlib.util.find_spec("pymysql") is not None:
            enable_platforms.append("mysql")
        if importlib.util.find_spec("psycopg2") is not None:
            enable_platforms.append("postgresql")
        return enable_platforms

    @staticmethod
    def use(ds_name: Optional[str] = None, **kwargs):
        """
        获取数据库对象
        :param ds_name: 数据源名称，默认为default
        :param kwargs: 数据源参数，根据数据源类型不同，参数也不同，如：
            通用参数：platform, host, port, database, user, password、print_sql
            Oracle参数：oracle_client_dir
        """
        db_config = get_config("db", {}).copy()

        # 填充自定义参数
        if kwargs:
            for k, v in kwargs.items():
                if 'data_source' == str(k):
                    if not isinstance(v, dict):
                        raise ValueError("参数data_source必须是字典类型！")
                    for data_source_name, data_source_value in v.items():
                        if not isinstance(data_source_value, dict):
                            raise ValueError("参数data_source的值必须是字典类型！")
                        if db_config.get("data_source") is None:
                            db_config["data_source"] = {}
                        db_config["data_source"][data_source_name] = data_source_value
                else:
                    db_config[k] = v

        # 数据源
        data_sources = db_config.get("data_source")
        if data_sources is None:
            raise ValueError("数据源配置不存在！")
        else:
            db_config.pop("data_source")
        data_source_name = 'default'
        if ds_name is not None and len(ds_name) > 0:
            data_source_name = ds_name
        data_source = data_sources.get(data_source_name)
        if data_source is None:
            raise ValueError(f"数据源[{data_source_name}]不存在！")

        # 根据数据源类型创建对应的数据库对象
        platform = data_source.get("platform", 'mysql')
        if platform not in Db.get_enabled_platforms():
            raise ValueError(f"不支持的数据源[{platform}]！")
        if platform == 'mysql':
            return MysqlDb(db_config, data_source)
        elif platform == 'postgres':
            return PostgresDb(db_config, data_source)
        elif platform == 'oracle':
            return OracleDb(db_config, data_source)
        else:
            raise ValueError(f"不支持的数据源[{platform}]！")


class MysqlDb(Db):

    def __init__(self, db_config: dict, data_source: dict):
        super().__init__(db_config, data_source)
        self.log = get_logger("MysqlDb")

    def open_connection(self, cursor_class=Cursor) -> tuple[pymysql.Connection, Cursor]:
        conn = pymysql.Connect(host=self.data_source.get('host'), port=int(self.data_source.get('port')),
                               user=self.data_source.get('username'), password=self.data_source.get('password'),
                               database=self.data_source.get('database'), cursorclass=cursor_class)
        if conn is None:
            raise ValueError("数据库连接失败！")
        return conn, conn.cursor(cursor_class)

    def query(self, sql, cursor_class=Cursor, fetch_type='all'):
        # 打印SQL
        if self.is_print_sql:
            self.log.info("sql: {}".format(sql))
        conn, cursor = self.open_connection(cursor_class)
        try:
            # 执行SQL并获取结果
            cursor.execute(sql)
            if fetch_type == 'all':
                fetch_results = cursor.fetchall()
            elif fetch_type == 'one':
                fetch_results = cursor.fetchone()
            elif fetch_type == 'many':
                fetch_results = cursor.fetchmany()
            else:
                fetch_results = None
        except Exception as e:
            raise e
        finally:
            cursor.close()
            conn.close()
        return fetch_results

    def query_all(self, sql) -> List[dict]:
        return self.query(sql, cursor_class=DictCursor, fetch_type='all')

    def query_one(self, sql) -> dict:
        return self.query(sql, cursor_class=DictCursor, fetch_type='one')

    def query_for_int(self, sql) -> Optional[int]:
        result = self.query(sql, fetch_type='one')
        int_result = result[0]
        if int_result is None:
            return None
        if isinstance(int_result, int):
            return int_result
        else:
            return int(float(int_result))

    def query_for_str(self, sql) -> Optional[str]:
        result = self.query(sql, fetch_type='one')
        str_result = result[0]
        if str_result is None:
            return None
        if isinstance(str_result, str):
            return str_result
        elif isinstance(str_result, datetime.datetime):
            return str_result.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(str_result, datetime.date):
            return str_result.strftime('%Y-%m-%d')
        else:
            return str(str_result)

    def query_for_bool(self, sql):
        result = self.query(sql, fetch_type='one')
        bool_result = result[0]
        if bool_result is None:
            return None
        if isinstance(bool_result, bool):
            return bool_result
        else:
            return str(bool_result).lower() == 'true'

    def query_for_float(self, sql) -> Optional[float]:
        result = self.query(sql, fetch_type='one')
        float_result = result[0]
        if float_result is None:
            return None
        if isinstance(float_result, float):
            return float_result
        else:
            return float(float_result)

    def update(self, sql) -> int:
        if self.is_print_sql:
            print(sql)
        conn, cursor = self.open_connection(DictCursor)
        try:
            cursor.execute(sql)
            affected_count = cursor.rowcount
            conn.commit()
        except Exception as e:
            conn.rollback()
            raise e
        finally:
            cursor.close()
            conn.close()
        return affected_count


class OracleDb(Db):

    def __init__(self, db_config: dict, data_source: dict):
        super().__init__(db_config, data_source)
        self.log = get_logger("OracleDb")
        # 初始化oracle_client
        oracle_client_dir = self.db_config.get("oracle_client_dir")
        if oracle_client_dir:
            cx_Oracle.init_oracle_client(lib_dir=oracle_client_dir)

    def open_connection(self):
        # 数据数据库连接
        user = self.data_source.get('username')
        password = self.data_source.get('password')
        host = self.data_source.get('host')
        port = self.data_source.get('port')
        database = self.data_source.get('database')
        db_conn, db_cursor = cx_Oracle.connect(user, password, f"{host}:{port}/{database}")
        return db_conn, db_cursor

    def query(self, sql, to_dict: bool = False, fetch_type='all'):
        db_conn, db_cursor = self.open_connection()
        # 打印SQL
        if self.is_print_sql:
            self.log.info("sql: {}".format(sql))
        # 执行SQL并获取结果
        db_cursor.execute(sql)
        # 获取结果
        results = []
        if to_dict:
            column_names = [row[0] for row in db_cursor.description]
            for row in db_cursor:
                results.append(dict(zip(column_names, row)))
        else:
            results = db_cursor.fetchall()
        # 关闭数据库连接
        db_cursor.close()
        db_conn.close()
        # 返回结果
        if fetch_type == 'all':
            return results
        elif fetch_type == 'one':
            return results[0] if len(results) > 0 else None
        else:
            return None

    def query_list(self, sql) -> list[dict]:
        return self.query(sql, to_dict=True, fetch_type='all')

    def query_one(self, sql) -> dict:
        return self.query(sql, to_dict=True, fetch_type='one')

    def query_for_int(self, sql) -> Optional[int]:
        result: tuple = self.query(sql, fetch_type='one')
        int_result = result[0]
        if int_result is None:
            return None
        if isinstance(int_result, int):
            return int_result
        else:
            return int(float(int_result))

    def query_for_str(self, sql) -> Optional[str]:
        result: tuple = self.query(sql, fetch_type='one')
        str_result = result[0]
        if str_result is None:
            return None
        if isinstance(str_result, str):
            return str_result
        else:
            return str(str_result)

    def query_for_bool(self, sql) -> Optional[bool]:
        result: tuple = self.query(sql, fetch_type='one')
        bool_result = result[0]
        if bool_result is None:
            return None
        if isinstance(bool_result, bool):
            return bool_result
        else:
            return str(bool_result).lower() == 'true'

    def query_for_float(self, sql) -> Optional[float]:
        result: tuple = self.query(sql, fetch_type='one')
        float_result = result[0]
        if float_result is None:
            return None
        if isinstance(float_result, float):
            return float_result
        else:
            return float(float_result)

    def update(self, sql) -> int:
        if self.is_print_sql:
            print(sql)
        # 数据数据库连接
        db_conn, db_cursor = self.open_connection()
        try:
            db_cursor.execute(sql)
            affected_count = db_cursor.rowcount
            db_conn.commit()
        except Exception as e:
            db_conn.rollback()
            raise e
        finally:
            db_cursor.close()
            db_conn.close()
        return affected_count


class PostgresDb(Db):
    pass


if __name__ == '__main__':
    db = Db.use()
    print(db.query_for_int("select 1"))
