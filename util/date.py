# -*- coding: utf-8 -*-
from datetime import datetime, timedelta


def get_date_today(fmt='%Y-%m-%d') -> str:
    """Get today

    Args:
        :param fmt: Date format, default '%Y-%m-%d'

    Returns:
        str: Today in format '%Y-%m-%d'
    """
    return datetime.today().strftime(fmt)


def get_date_yesterday(fmt='%Y-%m-%d') -> str:
    """Get yesterday

    Args:
        :param fmt: Date format, default '%Y-%m-%d'

    Returns:
        str: Yesterday in format '%Y-%m-%d'
    """
    return date_add(get_date_today(fmt), -1)


def get_date_tomorrow(fmt='%Y-%m-%d') -> str:
    """Get next day

    Args:
        :param fmt: Date format, default '%Y-%m-%d'

    Returns:
        str: Next day in format '%Y-%m-%d'
    """
    return date_add(get_date_today(fmt), 1)


def date_add(date: str, days: int = 0, fmt: str = '%Y-%m-%d') -> str:
    """Get next day

    Args:
        :param date: Date in string format
        :param fmt: date format
        :param days: days to add

    Returns:
        str: Next day in format '%Y-%m-%d'
    """
    return (datetime.strptime(date, fmt) + timedelta(days=days)).strftime(fmt)


def is_workday(date: str, fmt: str = '%Y-%m-%d') -> bool:
    """Check if date is workday

    Args:
        :param date: Date in string format
        :param fmt: date format
    Returns:
        bool: True if date is workday
    """
    return datetime.strptime(date, fmt).weekday() < 5


def is_weekend(date: str, fmt: str = '%Y-%m-%d') -> bool:
    """Check if date is weekend

    Args:
        :param date: Date in string format
        :param fmt: date format

    Returns:
        bool: True if date is weekend
    """
    return datetime.strptime(date, fmt).weekday() >= 5


def get_next_workday(date: str, fmt: str = '%Y-%m-%d') -> str:
    """Get next workday

    Args:
        :param date: Date in string format
        :param fmt: date format

    Returns:
        str: Next workday in format '%Y-%m-%d'
    """
    return date_add_workday(date, 1, fmt)


def get_previous_workday(date: str, fmt: str = '%Y-%m-%d') -> str:
    """Get previous workday

    Args:
        :param date: Date in string format
        :param fmt: date format

    Returns:
        str: Previous workday in format '%Y-%m-%d'
    """
    return date_add_workday(date, -1, fmt)


def date_add_workday(date: str, days: int, fmt: str = '%Y-%m-%d') -> str:
    """Get next workday

    Args:
        :param date: Date in string format
        :param fmt: date format
        :param days: days to add

    Returns:
        str: Next workday in format '%Y-%m-%d'
    """
    if days > 0:
        for idx in range(days):
            date = date_add(date, 1, fmt)
            while not is_workday(date, fmt):
                date = date_add(date, 1, fmt)
    elif days < 0:
        for idx in range(abs(days)):
            date = date_add(date, -1, fmt)
            while not is_workday(date, fmt):
                date = date_add(date, -1, fmt)
    return date


def get_next_weekend(date: str, fmt: str = '%Y-%m-%d') -> str:
    """Get next weekend

    Args:
        :param date: Date in string format
        :param fmt: date format

    Returns:
        str: Next weekend in format '%Y-%m-%d'
    """
    return date_add_weekend(date, 1, fmt)


def get_previous_weekend(date: str, fmt: str = '%Y-%m-%d') -> str:
    """Get previous weekend

    Args:
        :param date: Date in string format
        :param fmt: date format

    Returns:
        str: Previous weekend in format '%Y-%m-%d'
    """
    return date_add_weekend(date, -1, fmt)


def date_add_weekend(date: str, days: int, fmt: str = '%Y-%m-%d') -> str:
    """Get next weekend

    Args:
        :param date: Date in string format
        :param fmt: date format
        :param days: days to add

    Returns:
        str: Next weekend in format '%Y-%m-%d'
    """
    if days > 0:
        for idx in range(days):
            date = date_add(date, 1, fmt)
            while not is_weekend(date, fmt):
                date = date_add(date, 1, fmt)
    elif days < 0:
        for idx in range(abs(days)):
            date = date_add(date, -1, fmt)
            while not is_weekend(date, fmt):
                date = date_add(date, -1, fmt)
    return date
