# -*- coding: utf-8 -*-
import pyautogui
from pyautogui import Window

import time


def get_windows() -> list[Window]:
    """
    获取当前所有的窗口
    """
    return pyautogui.getAllWindows()


def get_window_with_title(title: str) -> Window or None:
    """
    根据窗口标题获取窗口
    """
    windows = pyautogui.getWindowsWithTitle(title)
    if windows:
        return windows[0]
    return None


def get_box_on_screen_by_img(img_path):
    """
    根据指定的图片，获取在屏幕上的位置（Box(left, top, width, height)）
    """
    return pyautogui.locateOnScreen(img_path)


def get_point_on_screen_by_img(img_path):
    """
    根据指定的图片，获取在屏幕上的位置（Point(x, y)）
    """
    pos = pyautogui.locateOnScreen(img_path)
    if pos is None:
        return None
    return pyautogui.center(pos)


def click_on_screen_by_img(img_path: str, pause: int = 1, clicks: int = 1,
                           button: str = 'left', duration: float = 0.25) -> bool:
    """
    根据指定的图片，点击屏幕上的位置
    :param img_path: 图片路径
    :param pause: 点击后暂停的时间
    :param clicks: 点击次数
    :param button: 鼠标按键
    :param duration: 点击持续时间
    :return: 是否点击成功
    """
    pos = pyautogui.locateOnScreen(img_path)
    if pos is None:
        return False
    pyautogui.click(pyautogui.center(pos), clicks=clicks, button=button, duration=duration)
    if pause > 0:
        time.sleep(pause)
    return True
