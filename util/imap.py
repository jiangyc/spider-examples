# -*- coding: utf-8 -*-
from datetime import datetime
import email
from email.header import decode_header
from email.message import Message
from typing import List, Dict, Any

from imapclient import IMAPClient, version
from imapclient.response_types import Envelope


class Imap:

    def __init__(self, host: str, ssl: bool = True, port: int = 993):
        self.server: IMAPClient = IMAPClient(host=host, port=port, ssl=ssl)

    def login(self, username: str, password: str = None, safe_id: bool = False):
        """
        登录
        :param username: 用户名，一般是邮箱
        :param password: 密码
        :param safe_id: 是否设置ID(某些邮箱，如163，需要设置ID才能访问)
        """
        # 登录
        self.server.login(username, password)
        # 设置ID
        if safe_id:
            self.server.id_({"name": "IMAPClient", "version": version.version})

    def logout(self):
        """
        退出登录
        :return:
        """
        if self.server is not None:
            self.server.logout()

    def folders(self) -> List[str]:
        """
        获取所有目录
        :return: 目录列表
        """
        folder_names = []
        folders = self.server.list_folders()
        for folder in folders:
            folder_name = folder[2]
            folder_names.append(folder_name)
        return folder_names

    def search(self, folder, criteria, charset=None) -> List[int]:
        """
        根据指定的检索条件，在服务器上检索邮件
        :param folder: 目录
        :param criteria: 检索条件
        :param charset: 字符集
        """
        self.server.select_folder(folder, readonly=True)
        # 搜索
        return self.server.search(criteria, charset)

    def _fetch_envelope(self, messages: int or list[int], folder: str or None) -> dict[int, Envelope] or None:
        """
        获取邮件的Envelope
        :param messages: 邮件uid，int类型或者list[int]类型
        :param folder: 邮件所在目录
        :return: dict[uid, envelope]
        """
        # 如果folder是空的，则使用默认目录
        target_folders = []
        if folder is None:
            target_folders = self.folders()
        else:
            target_folders.append(folder)

        # 遍历目录，查询Envelope
        target_envelopes = {}
        for target_folder in target_folders:
            self.server.select_folder(target_folder, readonly=True)
            # 查询envelopes
            fetch_envelopes = self.server.fetch(messages, ['ENVELOPE'])
            if fetch_envelopes is None:
                continue
            else:
                print("_fetch_envelope，查询到envelope，目录：", target_folder)
                for uid, envelope in fetch_envelopes.items():
                    target_envelopes[uid] = envelope.get(b'ENVELOPE')
                break
        return target_envelopes

    def fetch_envelope(self, messages: int or List[int], folder: str or None = None) -> Dict[int, Any] or None:
        print(f"========== fetch_envelope {messages} ==========")
        # 查询Envelope
        envelopes = self._fetch_envelope(messages, folder)
        if envelopes is None or len(envelopes) == 0:
            return None
        # 解析Envelope
        mails = {}
        for uid, envelope in envelopes.items():
            mail = {
                "uid": uid,
                "send_time": envelope.date.strftime('%Y-%m-%d %H:%M:%S'),
            }
            mails[uid] = mail
            # date
            # message_id
            message_id = envelope.message_id
            message_id = message_id.decode() if isinstance(message_id, bytes) else str(message_id)
            mail["message_id"] = message_id
            # subject
            subject = self.decode_str(envelope.subject.decode())
            mail["subject"] = subject
            # sender
            senders = envelope.sender
            if senders is not None and len(senders) > 0:
                sender = senders[0]
                if sender.name is not None:
                    mail["sender_name"] = self.decode_str(sender.name.decode())
                if sender.mailbox is not None and sender.host is not None:
                    mail["sender_email"] = f'{sender.mailbox.decode()}@{sender.host.decode()}'
        return mails

    def fetch(self, uid: int or list[int], folder: str or None = None) -> Dict[int, dict] or None:
        """
        根据UID获取邮件详细信息
        :param uid: 邮件的UID
        :param folder: 邮件的目录
        :return: 邮件详细信息
        """
        print(f"========== fetch {uid} ==========")
        envelope_mails = self.fetch_envelope(uid, folder)
        if envelope_mails is None or len(envelope_mails) == 0:
            return None

        for uid, mail in envelope_mails.items():
            email_message: Message = email.message_from_bytes(self.server.fetch(uid, ['RFC822'])[uid].get(b'RFC822'))
            mail["message"] = email_message
            maintype = email_message.get_content_maintype()
            if maintype == 'text':
                email_content = email_message.get_payload(decode=True)
                if email_content is not None:
                    email_content = email_content.strip()
                    email_content = self.decode_bytes(email_content)
                    mail["content"] = email_content
            elif maintype == 'multipart':
                contents: list[str] = []
                for part in email_message.walk():
                    part_maintype = part.get_content_maintype()
                    if part_maintype == 'text':
                        # part_content_type:
                        #     # text/plain
                        #     # text/html
                        email_content = part.get_payload(decode=True)
                        if email_content is not None:
                            email_content = email_content.strip()
                            email_content = self.decode_bytes(email_content)
                            contents.append(email_content)
                    elif part_maintype == 'application':
                        # part_content_type:
                        #     # application/txt
                        #     # application/zip
                        #     # application/pdf
                        #     # application/octet-stream
                        #     # application/vnd.ms-excel
                        #     # application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
                        filename = part.get_filename()
                        if filename is None:
                            continue
                        filename = self.decode_str(filename)
                        attachment = part.get_payload(decode=True)
                        mail_attachments = mail.get("attachments", {})
                        mail_attachments[filename] = attachment
                        mail["attachments"] = mail_attachments
                if len(contents) > 0:
                    mail["content"] = '\n'.join(contents)
        return envelope_mails

    @staticmethod
    def decode_bytes(data: bytes, charset: str = None) -> str:
        """
        解码bytes，默认使用utf-8，如果解码失败，使用GBK，
        :param charset: 解码使用的字符集
        :param data: 要解码的bytes
        :return:
        """
        if charset is not None:
            return data.decode(charset)

        decoded_data = None
        encodings = ['UTF-8', 'GBK', 'GB2312', 'GB18030', 'BIG5']
        for encoding in encodings:
            try:
                decoded_data = data.decode(encoding)
                break
            except UnicodeDecodeError:
                pass
        if decoded_data is None:
            print('Failed to decode using the following encoding: ', encodings)
        return decoded_data

    @staticmethod
    def decode_str(header: str) -> str or None:
        """
        解析字符串
        :param header: 要解析的字符串
        :return: 解码后的字符串
        """
        if header is None:
            return None
        decs = decode_header(header)

        str_list = []

        for dec in decs:
            # 如果是bytes，则解码
            if isinstance(dec[0], bytes):
                str_list.append(Imap.decode_bytes(dec[0]))
            # 如果是str，则直接返回
            elif isinstance(dec[0], str):
                str_list.append(dec[0])

        return ''.join(str_list)


if __name__ == "__main__":
    imap = Imap(host="imap.163.com")

    fds = imap.folders()
    for fd in fds:
        print(fd)
    print("共有{}个目录".format(len(fds)))

    # 当天日期，格式：31-Mar-2023
    print("--------------------------------------------------------------")
    today = datetime.now().strftime("%d-%b-%Y")
    mail_ids = imap.search(folder="INBOX", criteria=["ON", today])
    print(mail_ids)

    print("--------------------------------------------------------------")
    for mail_id in mail_ids:
        mail_info = imap.fetch_envelope(mail_id, 'INBOX')
        print(mail_info)
        mail_info_full = imap.fetch(mail_id, 'INBOX')
        print(mail_info_full)
        break


