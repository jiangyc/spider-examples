# -*- coding: utf-8 -*-
"""
Controller所在包

示例：
    from controller import global_controller


    @global_controller.route("/hello", methods=["GET"])
    def hello():
        return "Hello world!"
"""
from flask import Blueprint


global_controller = Blueprint('global_controller', __name__)


__all__ = [
    "global_controller",
    "hello_controller",
]
