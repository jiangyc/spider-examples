# -*- coding: utf-8 -*-
from controller import global_controller


@global_controller.route("/hello", methods=["GET"])
def hello() -> str:
    return "Hello world!"
