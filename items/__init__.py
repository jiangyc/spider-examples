# -*- coding: utf-8 -*-
from typing import Optional

from util import Db


class Item:
    # 定义公共字段：table_name
    __table_name__: str or None = None

    # 定义表主键
    __primary_keys__: list[str] = []

    # 定义表字段
    __data__: dict = {}

    __ignored_data__: dict = {}

    # 定义数据库对象
    __db__: Db or None = None

    def __init__(self, table_name: str = None, primary_keys: list[str] = None, **kwargs):
        self.__table_name__ = None
        self.__primary_keys__ = []
        self.__data__ = {}
        self.__ignored_data__ = {}
        if table_name:
            self.__table_name__ = table_name
        if primary_keys:
            self.__primary_keys__.extend(primary_keys)
        if kwargs:
            self.__data__.update(kwargs)

    def set_table_name(self, table_name: str):
        self.__table_name__ = table_name

    def set_primary_keys(self, primary_keys: list[str]):
        self.__primary_keys__ = primary_keys

    def set_data(self, data: dict):
        if data:
            self.__data__ = data

    def set_ignored_data(self, ignored_data: dict):
        if ignored_data:
            self.__ignored_data__ = ignored_data

    def __str__(self):
        return str(self.data)

    @property
    def table_name(self):
        return self.__table_name__

    @property
    def primary_keys(self):
        return self.__primary_keys__

    @property
    def data(self):
        return self.__data__

    @property
    def ignored_data(self):
        return self.__ignored_data__

    @property
    def db(self):
        if not self.__db__:
            self.__db__ = Db.use()
        return self.__db__

    def get(self, key):
        return self.data.get(key)

    def get_ignored(self, key):
        return self.ignored_data.get(key)

    def set(self, **kwargs):
        self.data.update(kwargs)

    def set_ignored(self, **kwargs):
        self.ignored_data.update(kwargs)

    def clear(self):
        self.data.clear()

    def smart_add(self, ignore_none: bool = False, db: Optional[Db] = None) -> bool:
        """
        智能添加数据
        :param ignore_none: 是否忽略空值
        :param db: 数据库对象
        :return: 是否成功
        """
        if db:
            return db.smart_add(self.table_name, self.data, ignore_none)
        return self.db.smart_add(self.table_name, self.data, ignore_none)

    def smart_update(self, where: Optional[dict] = None, where_sql: Optional[str] = None, ignore_none: bool = False,
                     db: Optional[Db] = None) -> bool:
        """
        智能更新数据
        :param where: 条件，字典。当where_sql不为空时，where参数无效
        :param where_sql: 条件SQL，字符串。当where_sql不为空时，where参数无效
        :param ignore_none: 是否忽略数据中的空值
        :param db: 数据库对象
        :return: 是否成功
        """
        # 如果没有where条件，则取主键作为条件
        primary_key_where = where
        if not where and not where_sql:
            primary_key_where = {}
            for key in self.primary_keys:
                value = self.data.get(key)
                if value is None:
                    raise Exception("主键值不能为空！")
                primary_key_where[key] = value
        if db:
            return db.smart_update(self.table_name, self.data, primary_key_where, where_sql, ignore_none)
        return self.db.smart_update(self.table_name, self.data, primary_key_where, where_sql, ignore_none)

    def smart_add_or_update(self, where: Optional[dict] = None, where_sql: Optional[str] = None, ignore_none: bool = False,
                            db: Optional[Db] = None) -> bool:
        """
        智能添加或更新数据
        :param where: 条件，字典。当where_sql不为空时，where参数无效
        :param where_sql: 条件SQL，字符串。当where_sql不为空时，where参数无效
        :param ignore_none: 是否忽略数据中的空值
        :param db: 数据库对象
        :return: 是否成功
        """
        # 如果没有where条件，则取主键作为条件
        primary_key_where = where
        if not where and not where_sql:
            primary_key_where = {}
            for key in self.primary_keys:
                value = self.data.get(key)
                if value is None:
                    raise Exception("主键值不能为空！")
                primary_key_where[key] = value
        if db:
            return db.smart_add_or_update(self.table_name, self.data, primary_key_where, where_sql, ignore_none)
        return self.db.smart_add_or_update(self.table_name, self.data, primary_key_where, where_sql, ignore_none)
