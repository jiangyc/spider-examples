
# Build

```bash
python -m venv .venv

source ./.venv/bin/activate

pip install -r ./requirements.txt

python main.py
```

# Dependencies

* [selenium](https://www.selenium.dev/) : A browser automation framework and ecosystem
* [Flask](https://github.com/pallets/flask/) : A simple framework for building complex web applications.
* [APScheduler](https://github.com/agronholm/apscheduler) : In-process task scheduler with Cron-like capabilities
* [Flask-APScheduler](https://github.com/viniciuschiele/flask-apscheduler) : Adds APScheduler support to Flask
* [PyMySQL](https://github.com/PyMySQL/PyMySQL) : Pure Python MySQL Driver
* [PyAutoGUI](https://github.com/asweigart/pyautogui) : A cross-platform GUI automation Python module for human beings. Used to programmatically control the mouse & keyboard.
* [Pillow](https://python-pillow.org/) : Python Imaging Library (Fork)
* [cx-Oracle](https://oracle.github.io/python-cx_Oracle) : Python interface to Oracle
* [psycopg2-binary](https://psycopg.org/) : psycopg2 - Python-PostgreSQL Database Adapter

# Suggestions

* [ddddocr](https://github.com/sml2h3/ddddocr) (>= 1.4.7) : 带带弟弟 通用验证码识别OCR pypi版
* [CnOCR](https://github.com/breezedeus/cnocr) (>= 2.2.2.1) : Python3 package for Chinese/English OCR, with small pretrained modules
* [pywin32](https://github.com/mhammond/pywin32) (>= 305) : Python for Window Extensions
