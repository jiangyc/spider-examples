# -*- coding: utf-8 -*-
from job import global_scheduler


@global_scheduler.task('cron', id='hello_job', day='*', hour='*', minute='0', second='0')
def hello():
    print("Hello world!")
