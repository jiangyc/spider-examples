# -*- coding: utf-8 -*-
"""
Job所在包

示例：
    from job import global_scheduler


    @global_scheduler.task('cron', id='hello_job', day='*', hour='*', minute='0', second='0')
    def hello():
        print("Hello world!")
"""


from flask_apscheduler import APScheduler
from apscheduler.schedulers.background import BackgroundScheduler

global_scheduler = APScheduler(scheduler=BackgroundScheduler(timezone='Asia/Shanghai'))

__all__ = [
    "global_scheduler",
    "hello_job",
]
