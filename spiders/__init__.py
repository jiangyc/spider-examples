# -*- coding: utf-8 -*-
"""
爬虫的基本类，包含了一些工具方法
"""
import datetime

from util import get_logger


class BaseSpider:
    """
    默认爬虫类
    """

    def __init__(self, spider_name: str):
        self.start_time: str or None = None
        self.finish_date: str or None = None
        self.spider_name = spider_name
        self.log = get_logger(spider_name)

    def start(self):
        self.start_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.before_run()

        try:
            self.run()
        except Exception as e:
            self.error_run(e)

        self.finish_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.finish_run()

    def before_run(self):
        self.log.info("spider [{}] start at: {}".format(self.spider_name, self.start_time))

    def error_run(self, e: Exception):
        self.log.error(e)
        pass

    def finish_run(self):
        self.log.info("spider [{}] finish at: {}".format(self.spider_name, self.start_time))
        pass

    def run(self):
        pass
